class NotesController < ApplicationController
    def index
        @q = Post.ransack(params[:q])
        @posts = @q.result(distinct: true)
    end

    def show
      @post = Post.find(params[:id])
    end
end