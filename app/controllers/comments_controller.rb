class CommentsController < ApplicationController
    before_action :authenticate_user!

    def new
      @comment = current_user.posts.build
      @post = Post.find(params[:id])
      @comment = @post.comments.build
    end

    def create
        if @comment.save
            format.html { redirect_to @post, notice: 'Comment was successfully created.' }
            format.json { render :show, status: :created, location: @comment }
          else
            format.html { render :new }
            format.json { render json: @comment.errors, status: :unprocessable_entity }
          end
    end


end
