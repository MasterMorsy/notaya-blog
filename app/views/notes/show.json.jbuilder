json.title @post.title
json.body @post.body
json.image @post.image.url
json.user @post.user
json.created_at @post.created_at
json.updated_at @post.updated_at  