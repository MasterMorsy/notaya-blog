# == Schema Information
#
# Table name: posts
#
#  id         :bigint(8)        not null, primary key
#  title      :string
#  body       :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :integer
#

class Post < ApplicationRecord
    belongs_to :user
    has_many :comments
    belongs_to :category
    validates :title, presence: true, uniqueness: {case_sensitive: false}, length: {minimum:5, maximum: 150}
    validates :category_id, presence: true
    validates :body, presence: true, length: {minimum:10, maximum: 5000}

    #add image to post
    has_attached_file :image, styles: { large: "600x600>", medium: "300x300>", small: "100x100>" }, default_url: "/images/:style/missing.png"
    validates_attachment_content_type :image, content_type: ["image/jpeg", "image/gif", "image/png"]
end
